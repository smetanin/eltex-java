import javax.swing.*;
import java.util.Random;

public class BrickHouse extends Home {
    /*public int x;
    public int y;
    public double tob;
    public double ttl;
    */
    @Override
    public void draw_house(){
        //image = new ImageIcon("index.jpeg").getImage();
        System.out.println("BrickHouse x=" + x + " y=" +y);
    }

    @Override
    public int getPointX(int x) {
        Random r = new Random();
        this.x = r.nextInt(x);
        return this.x;
    }

    @Override
    public int getPointY(int y) {
        Random r = new Random();
        this.y = r.nextInt(y);
        return this.y;
    }

    @Override
    public int draw_time() {
        return 0;
    }

    @Override
    public double set_tob(double tob){
        this.tob = tob;
        return 0;
    }

    @Override
    public int set_ttl(int ttl){
        this.ttl = ttl;
        return 0;
    }
}
