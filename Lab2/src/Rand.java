import java.util.Random;
import java.util.TimerTask;

public class Rand extends TimerTask{
    public int x;
    public int y;
    public int getPointX(int x){
        Random r = new Random();
         this.x = r.nextInt(x);
        return this.x;
    }

    public int getPointY(int y){
        Random r = new Random();
        this.y = r.nextInt(y);
        return this.y;
    }
    
    public void run(){
        System.out.println("Hello RAND");
    }
}
