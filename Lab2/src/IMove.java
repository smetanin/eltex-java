interface IMove {
    public int getPointX(int x);
    public int getPointY(int y);
    public int draw_time();
    public double set_tob(double tob);
    public int set_ttl(int ttl);
}
