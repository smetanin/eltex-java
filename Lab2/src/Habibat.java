import com.sun.deploy.panel.ITreeNode;

import java.awt.Graphics;
import java.util.*;
import javax.swing.JPanel;

public class Habibat extends JPanel{
    
    public Habibat ths;
    private Vector<Home> homes;
    private HashSet<HomeID> homeID;
    private HashSet<Home> hashid;
    private HashSet<Integer> idhash;
    public float pB;
    public float pP;
    
    public void setHomes(Vector<Home> h){
        homes = h;
    }

    public void setHomeID(HashSet<HomeID> hid){
        homeID = hid;
    }
    
        public class Updater extends TimerTask     {
        private ImgPanel im_panel = null;
        // Первый ли запуск метода run()?
        private boolean m_firstRun = true;
        // Время начала
        private long m_startTime = 0;
        // Время последнего обновления
        private long m_lastTime = 0;


        @Override
        public void run()     {
            if (m_firstRun)       {
                m_startTime = System.currentTimeMillis();
                m_lastTime = m_startTime;
                m_firstRun = false;
            }
            long currentTime = System.currentTimeMillis();
            // Время, прошедшее от начала, в секундах
            double elapsed = (currentTime - m_startTime) / 1000.0;
            // Время, прошедшее с последнего обновления, в секундах
            double frameTime = (m_lastTime - m_startTime) / 1000.0;
            // Вызываем обновление

            m_lastTime = currentTime;

            for (int i=0; i<homes.size(); i++){
                if (homes.get(i).ttl + homes.get(i).tob < elapsed){
                    System.out.println("\nУДАЛЕН ДОМ ПОД ИНДЕКСОМ = " + i + " name = " + homes.get(i));

                    //homeID.remove(i);
                    Iterator<Integer> iter = idhash.iterator();
                    try{
                        while(iter.hasNext()){
                            if (iter.next().equals(homes.get(i).id)){
                                iter.remove();
                            }
                        }
                    }
                    catch (Exception e){

                    }
                    homes.remove(i);
                }
            }

            Random r = new Random();
            HomeID hid = new HomeID();
            BrickHouse br = new BrickHouse();
            br.set_tob(elapsed);
            br.set_ttl(r.nextInt(80));
            br.getPointX(Main.x);
            br.getPointY(Main.y);
            addHomes(br);
            br.id = r.nextInt(10000);
            idhash.add(br.id);
            hid.addHome((int)elapsed,br.id);    //добавление элементов tod,id в класс HomeID
            addHomeID(hid);                     //добавление объекта HomeID в HashSet "homeID"
            //hashid.add(br);
            addHashID(br);
            System.out.println("\nadd BrickHouse" + "\nВремя рождения = " + br.tob + " сек. \nВремя жизни = " + br.ttl + " сек.\nid = " + hid.id );

            br.draw_house();

/*
            PanelHouse ph = new PanelHouse();
            ph.set_tob(elapsed);
            ph.set_ttl(r.nextInt(80));
            ph.getPointX(Main.x);
            ph.getPointY(Main.y);
            addHomes(ph);
            ph.id = r.nextInt(100);
            hid.addHome((int)elapsed,r.nextInt(100));
            addHomeID(hid);
            hashid.add(ph);
            System.out.println("\nadd PanelHouse" + "\nВремя рождения = " + ph.tob + " сек. \nВремя жизни = " + ph.ttl + " сек.");
            ph.draw_house();
*/



            if (elapsed >= 50) {
                for (int i = 0; i<homes.size(); i++){
                    System.out.println(i + " Homes tob = " + homes.get(i).tob + " homes id = " + homes.get(i).id);
                }

                Iterator<HomeID> it = homeID.iterator();
                try{
                    while(it.hasNext())
                        System.out.println("tob = " + it.next().tob + " id = " + it.next().id);
                }
                catch (Exception e){

                }

                Iterator<Home> ith = hashid.iterator();
                try{
                    while(ith.hasNext())
                        System.out.println("HashID tob = " + ith.next().tob + " HashID id = " + ith.next().id);
                }
                catch (Exception e){

                }

                Iterator<Integer> iterh = idhash.iterator();
                try{
                    while(iterh.hasNext())
                        System.out.println("int id = " + iterh.next());
                }
                catch (Exception e){

                }

            }
        }
    }

    Habibat(){
        homes = new Vector<Home>();
        homeID = new HashSet<HomeID>();
        hashid = new HashSet<Home>();
        idhash = new HashSet<Integer>();
        ths = this;
    }
    
    public void showHomes(){
        for (int i=0; i<homes.size(); i++){
            Home h = homes.get(i);
            System.out.println("showHomes "+ h.getName() + " x = " + h.x + " y = " + h.y);
        }
    }
    
    public void addHomes(Home home){
        homes.add(home);
    }

    public void addHomeID(HomeID home){
        homeID.add(home);
    }

    public void addHashID(Home home){
        hashid.add(home);
    }
}