import javax.swing.*;
import java.awt.*;

abstract class Home extends JPanel implements IMove{
    public Image image;
    public int x;
    public int y;
    public double tob;
    public double ttl;
    public int id;

   abstract public void draw_house();
}