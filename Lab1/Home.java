import javax.swing.*;
import java.awt.*;

abstract class Home extends JPanel implements IMove{
    public Image image;
    public int x;
    public int y;

   abstract public void draw_house();
}