import java.awt.Graphics;
import java.util.ArrayList;
import java.util.TimerTask;
import javax.swing.JPanel;

public class Habibat extends JPanel{
    
    public Habibat ths;
    private ArrayList<Home> homes;
    
 
    public void setHomes(ArrayList<Home> h){
        
        homes = h;
    }
    
        public class Updater extends TimerTask     {
        private ImgPanel im_panel = null;
        // Первый ли запуск метода run()?
        private boolean m_firstRun = true;
        // Время начала
        private long m_startTime = 0;
        // Время последнего обновления
        private long m_lastTime = 0;


        @Override
        public void run()     {
            if (m_firstRun)       {
                m_startTime = System.currentTimeMillis();
                m_lastTime = m_startTime;
                m_firstRun = false;
            }
            long currentTime = System.currentTimeMillis();
            // Время, прошедшее от начала, в секундах
            double elapsed = (currentTime - m_startTime) / 1000.0;
            // Время, прошедшее с последнего обновления, в секундах
            double frameTime = (m_lastTime - m_startTime) / 1000.0;
            // Вызываем обновление

            m_lastTime = currentTime;
           
            BrickHouse br = new BrickHouse();
            br.getPointX(Main.x);
            br.getPointY(Main.y);
            br.draw_house();
            addHomes(br);
            System.out.println("add BrickHouse");

            PanelHouse ph = new PanelHouse();
            ph.getPointX(Main.x);
            ph.getPointY(Main.y);
            ph.draw_house();
            addHomes(ph);
            System.out.println("add PanelHouse");
        }
    }

    Habibat(){
        homes = new ArrayList<Home>();
        ths = this;
    }
    
    public void showHomes(){
        for (int i=0; i<homes.size(); i++){
            Home h = homes.get(i);
            System.out.println("showHomes "+ h.getName() + " x = " + h.x + " y = " + h.y);
        }
    }
    
    public void addHomes(Home home){
        homes.add(home);
    }
}