import javax.swing.*;
import java.awt.*;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class ImgPanel extends JPanel{
    public boolean i1,i2=false;

    public void setI1(boolean i){
        i1 = i;
    }
    public void setI2(boolean i){
        i2 = i;
    }

    private class Updater extends TimerTask     {
        private ImgPanel im_panel = null;
        // Первый ли запуск метода run()?
        private boolean m_firstRun = true;
        // Время начала
        private long m_startTime = 0;
        // Время последнего обновления
        private long m_lastTime = 0;


        @Override
        public void run()     {
            if (m_firstRun)       {
                m_startTime = System.currentTimeMillis();
                m_lastTime = m_startTime;
                m_firstRun = false;
            }
            long currentTime = System.currentTimeMillis();
            // Время, прошедшее от начала, в секундах
            double elapsed = (currentTime - m_startTime) / 1000.0;
            // Время, прошедшее с последнего обновления, в секундах
            double frameTime = (m_lastTime - m_startTime) / 1000.0;
            // Вызываем обновление

            m_lastTime = currentTime;
        }
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        BrickHouse br = new BrickHouse();
        br.draw_house();
        br.getPointX(Main.x);
        br.getPointY(Main.y);

        PanelHouse ph = new PanelHouse();
        ph.draw_house();
        ph.getPointX(Main.x);
        ph.getPointY(Main.y);

        /*Timer timer = new Timer();
        timer.schedule(new Updater(),3000);
*/
        g.drawImage(br.image, br.x, br.y, this);
        g.drawImage(ph.image, ph.x, ph.y, this);
    }
}
