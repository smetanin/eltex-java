import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created by denis on 08.06.17.
 */
public class PanelHouse extends Home{
    @Override
    public void draw_house(){
        //image = new ImageIcon("index1.jpeg").getImage();
        System.out.println("PanelHouse x=" + x + " y=" +y);
    }

    @Override
    public int getPointX(int x) {
        Random r = new Random();
        this.x = r.nextInt(x);
        return this.x;
    }

    @Override
    public int getPointY(int y) {
        Random r = new Random();
        this.y = r.nextInt(y);
        return this.y;
    }

    @Override
    public int draw_time() {
        return 0;
    }
}
